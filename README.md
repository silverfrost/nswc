# Using NSWC with Silverfrost Fortran

Naval Surface Warfare Center (NSWC) Library of mathematics subroutines is a library of
'general purpose Fortran subroutines that provide a basic computational capability for a variety 
of mathematical activities. Emphasis has been placed on the transportability of the codes. 
Subroutines are available in the following areas: elementary operations, geometry, special functions, polynomials, vectors, matrices,
large dense systems of linear equations, banded matrices, sparse matrices, eigenvalues and
eigenvectors, l1 solution of linear equations, least-squares solution of linear equations, optimization, transforms, approximation of functions, curve fitting, surface fitting, manifold
fitting, numerical integration, integral equations, ordinary differential equations, partial
differential equations, and random number generation.'

Presented here are 32- and 64-bit DLLs containing the library that has been compiled for use with FTN95. In addition
the original source (nswc.for) is available so that you can use it either directly linked into your own executables or
used from another compiler.
    
## How to use   

There are two DLLs, one each for 32- and -64 bit programs. You need to link the appropriate one
for your code.
    
## Acknowledgements

These DLLs were created by Silverfrost forum user: Kenneth_Smith

## Additional

Included is a Readme.txt with more information including a demonstration program provided by Kenneth_Smith.
