! Test for NSWC library
! Last changed 17/07/2023
winapp
module test
use clrwin
implicit none

integer, parameter :: sp=kind(1.0), dp=kind(1.d0)

contains


subroutine TEST_SHELL
! Subroutine to test the NSWC routine SHELL.
! Also calls the NSWC routine RNOR.
  integer, parameter :: n = 10
  integer i
  real(sp) a(n)
  integer ix
  integer ierr
  
  write(*,'(/A)') 'Test NSWC subroutine SHELL'
  write(*,'(A)')  '--------------------------'
  write(*,'(/A)') 'Here we have a real array of 10 real elements populated'
  write(*,'(A)')  'by a call to the NSWC subroutine RNOR'
  write(*,'(/A)') 'The array is sorted by calling the NSWC suboutine SHELL.'
  write(*,'(A/)') 'The shell sort method is used.'

! Generate a set of random numbers in A
! Here we use the NSCW subroutine RNOR, for this test the 
! intrinsic RANDOM_NUMBER@ could also be used.
  ix = 1    ! Seed for RNOR
  call RNOR(ix,a,n,ierr)

! Write A to screen  
  write(*,'(/A)') 'Unsorted list array a'
  do i = 1, n
    write(*,'(I2,3X,F8.4)') i, a(i)
  end do
  
! Call SHELL to sore the contants of A in asending order  
  call SHELL(a,n)

! Write the sorted array A to the screen  
  write(*,'(/A)') 'Sorted list array a'
  do i = 1, n
    write(*,'(I2,3X,F8.4)') i, a(i)
  end do
  
  write(*,*)
  call wait
  
end subroutine TEST_SHELL


subroutine TEST_DSORT
! Subroutine to test the NSWC routine DSORT.
! Also call the NSWC routine DRNOR.
  integer, parameter :: n = 10
  integer i
  real(dp) b(n)
  integer ix
  integer ierr
  
  write(*,'(/A)') 'Test NSWC subroutine DSORT'
  write(*,'(A)')  '--------------------------'
  write(*,'(/A)') 'Here we have a double precision real array of 10 real'
  write(*,'(A)')  'elements populated by a call to NSWC routine DRNOR.'
  write(*,'(/A)') 'The array is sorted by calling the NSWC suboutine DSORT.'
  write(*,'(A/)') 'The shell sort method is used.'

! Generate a set of random numbers in B.
! Here we use the NSCW subroutine DRNOR, for this test the 
! intrinsic RANDOM_NUMBER@ could also be used.
  ix = 1
  call DRNOR(ix,b,n,ierr)

! Write B to screen    
  write(*,'(/A)') 'Unsorted list array b'
  do i = 1, n
    write(*,'(I2,3X,F8.4)') i, b(i)
  end do

! Call DSORT to sore the contents of A in asending order   
  call DSORT(b,n)

! Write the sorted array B to the screen  
  write(*,'(/A)') 'Sorted list array b'
  do i = 1, n
    write(*,'(I2,3X,F8.4)') i, b(i)
  end do
  
  write(*,*)
  call wait
  
end subroutine TEST_DSORT


subroutine TEST_QSORTD
! Subroutine to test the NSWC routine QSORTD
! Also calls the routines DRNOR and DORDER
  integer, parameter :: n = 10
  integer i
  real(dp) b(n)
  integer ind(n)
  integer ix
  integer ierr
  
  write(*,'(/A)') 'Test NSWC subroutine QSORTD'
  write(*,'(A)')  '---------------------------'
  write(*,'(/A)') 'Here we have a double precision real array of 10 real'
  write(*,'(A)')  'elements populated by a call to the NSWC routine DRNOR.'
  write(*,'(/A)') 'The array is sorted by calling the NSWC suboutine QSORT.'
  write(*,'(A)')  'The quick sort method is used.'
  write(*,'(A)')  'QSORTD does not change the order of the input array.'
  write(*,'(A)')  'Instead it returns an integer array of pointers.'
  write(*,'(A/)') 'The array is sorted by calling DORDER'
  
! Generate a set of random numbers in B  
! Here we use the NSCW subroutine DRNOR, for this test the 
! intrinsic RANDOM_NUMBER@ could also be used.
! ix is the seed for DRNOR
  ix=1
  call DRNOR(ix,b,n,ierr)

! Write B to screen
  write(*,'(/A)') 'Unsorted list array b'
  do i = 1, n
    write(*,'(I2,3X,F8.4)') i, b(i)
  end do

! Call QSORTD to find the pointers of B in asending order   
  call QSORTD(b,ind,n)

! Write B and pointer array to screen  
  write(*,'(/A)') 'Unsorted array and pointers returned by QSORTD'
  do i = 1, n
    write(*,'(I2,3X,F8.4, 3X, I2)') i, b(i), ind(i)
  end do

! Reorder B using pointers in IND  
  call DORDER(b,ind,n)
  
! Write sorted B to screen  
  write(*,'(/A)') 'Sorted list array b after call to DORDER'
  do i = 1, n
    write(*,'(I2,3X,F8.4)') i, b(i)
  end do
  
  write(*,*)
  call wait
end subroutine TEST_QSORTD


subroutine TEST_CBRT
! Subroutine to test the NSWC routine CBRT.
  integer i
  real(sp) x
  real(sp) cuberootx
  real(sp) CBRT         ! Function CBRT must be declared as real in calling routine

  write(*,'(/A)') 'Test NSWC function CBRT'
  write(*,'(A)')  '-----------------------'
  write(*,'(/A)') 'Here we calculate the cube root of a random real number.'
  write(*,'(A)')  'The returned root is then cubed. This is repeated 10 times.'
  
  do i = 1, 10 
    
!   Generate random number in range -100 to 100    
    x = random_range_sp(-100.0,100.0)
    
!   Find the cube root of X by calling CBRT    
    cuberootx = CBRT(x)

!   Write results to screen    
    write(*,'(/A,F8.4,A,F8.4)') 'The cube root of ', x, ' is ', cuberootx

!   Check that CUBEROOT**3 is X, writing results to screen
    write(*,'(F8.4,A,F8.4)') cuberootx, '**3 is ', cuberootx**3
    write(*,*) 
  end do
  
  write(*,*)
  call wait
end subroutine TEST_CBRT


subroutine TEST_DCBRT
! Subroutine to test the NSWC routine DCBRT.
  integer i
  real(dp) x
  real(dp) cuberootx
  real(dp) DCBRT         ! Function CBRT must be declared as real in calling routine

  write(*,'(/A)') 'Test NSWC function DCBRT'
  write(*,'(A)')  '------------------------'
  write(*,'(/A)') 'Here we calculate the cube root of a random real number.'
  write(*,'(A)')  'The returned root is then cubed. This is repeated 10 times.'
  
  do i = 1, 10 
    
!   Generate random number in range -100 to 100    
    x = random_range_dp(-100.d0,100.d0)
    
!   Find the cube root of X by calling CBRT    
    cuberootx = DCBRT(x)

!   Write results to screen    
    write(*,'(/A,F8.4,A,F8.4)') 'The cube root of ', x, ' is ', cuberootx

!   Check that CUBEROOT**3 is X, writing results to screen
    write(*,'(F8.4,A,F8.4)') cuberootx, '**3 is ', cuberootx**3
    write(*,*) 
  end do
  
  write(*,*)
  call wait
end subroutine TEST_DCBRT


subroutine TEST_ARTNQ
! Subroutine to test the NSWC routine ARTNQ.
  real(sp), parameter :: pi      = 4.0*atan(1.0)
  real(sp), parameter :: deg2rad = pi/180.0
  real(sp), parameter :: rad2deg = 1.0/deg2rad
  complex(sp) z(13)
  integer i
  real(sp) x
  real(sp) y
  real(sp) ARTNQ               ! Function ATRNQ must be declared as real in calling routine

  write(*,'(/A)') 'Test NSWC function ARTNQ(y, x)'
  write(*,'(A)')  '------------------------------'
  write(*,'(/A)') 'Here we consider the unit circuit in the x-y plane, drawn by'
  write(*,'(A)')  'rotating the vector (1,0) in 30 deg increments in the'
  write(*,'(A)')  'positive (anti-clockwise) direction.  The (x,y) coordinates'
  write(*,'(A)')  'are converted to their angular displacement in the range 0'
  write(*,'(A)')  'to 2pi by calling ARTNQ(y,x)'

! Generate the test points on the unit circle
  z(1) = (1.0,0.0)
  do i = 2, size(z)
    z(i) = z(i-1)*EXP(cmplx(0.0,1.0)*30.0*deg2rad)
  end do

! Write the test points and the results of calling ARTNQ to the screen  
  write(*,'(/A)') '   x         y       rad_ang   deg_ang'
  do i = 1, size(z)
    x = real(z(i))
    y = aimag(z(i))
    write(*,'(4(F8.3,2X))') x, y, ARTNQ(y,x), ARTNQ(y,x)*rad2deg
  end do
  
  write(*,*)
  call wait
end subroutine TEST_ARTNQ


subroutine TEST_DARTNQ
! Subroutine to test the NSWC routine DARTNQ.
  real(dp), parameter :: pi = 4.d0*atan(1.d0)
  real(dp), parameter :: deg2rad = pi/180.d0
  real(dp), parameter :: rad2deg = 1.d0/deg2rad
  complex(dp) z(13)
  integer i
  real(dp) x
  real(dp) y
  real(dp) DARTNQ      ! Function DATRNQ must be declared as real in calling routine

  write(*,'(/A)') 'Test NSWC function DARTNQ(y, x)'
  write(*,'(A)')  '-------------------------------'
  write(*,'(/A)') 'Here we consider the unit circuit in the x-y plane, drawn by'
  write(*,'(A)')  'rotating the vector (1,0) in 30 deg increments in the'
  write(*,'(A)')  'positive (anti-clockwise) direction.  The (x,y) coordinates'
  write(*,'(A)')  'are converted to their angular displacement in the range 0'
  write(*,'(A)')  'to 2pi by calling DARTNQ(y,x)'
  
! Generate the test points on the unit circle  
  z(1) = (1.d0,0.d0)
  do i = 2, size(z)
    z(i) = z(i-1)*EXP(cmplx(0.d0,1.d0,kind=dp)*30.d0*deg2rad)
  end do

! Write the test points and the results of calling DARTNQ to the screen   
  write(*,'(/A)') '   x         y       rad_ang   deg_ang' 
  do i = 1, size(z)
    x = real(z(i))
    y = aimag(z(i))
    write(*,'(4(F8.3,2X))') x, y, DARTNQ(y,x), DARTNQ(y,x)*rad2deg
  end do
  
  write(*,*)
  call wait
end subroutine TEST_DARTNQ


subroutine TEST_CPABS
! Subroutine to test the NSWC routine CPABS.
integer, parameter :: n = 10
  real(sp) x1(n)
  real(sp) y1(n)
  integer i
  real(sp) CPABS 
  
  write(*,'(/A)') 'Test NSWC function CPABS(x, y)'
  write(*,'(A)')  '------------------------------'
  write(*,'(/A)') 'Here we calculate the magnitude of a random vector by'
  write(*,'(A)')  'calling the routine CPABS(x,y).  This is also calculated'
  write(*,'(A)')  'by an alternative method.  The calculation is repeated 10'
  write(*,'(A)')  'times.'
  write(*,'(/A)')  '  x            y            CPABS(x,y)   Alternative'
  
  do i = 1, n
    x1(i) = random_range_sp(-100.0,100.0)
    y1(i) = random_range_sp(-100.0,100.0)
    write(*,'(5(F8.3,5X))') x1(i), y1(i), CPABS(x1(i), y1(i)), abs(cmplx(x1(i),y1(i)))
  end do
  
  write(*,*)
  call wait
end subroutine TEST_CPABS



subroutine TEST_DCPABS
! Subroutine to test the NSWC routine CPABS.
integer, parameter :: n = 10
  real(dp) x2(n)
  real(dp) y2(n)
  integer i
  real(dp) DCPABS
    
  write(*,'(/A)') 'Test NSWC function DCPABS(x, y)'
  write(*,'(A)') '-------------------------------'
  write(*,'(/A)') 'Here we calculate the magnitude of a random double precision'
  write(*,'(A)')  'vector by calling the routine DCPABS(x,y).  This is also'
  write(*,'(A)')  'calculated by an alternative method. The calculation is'
  write(*,'(A)')  'repeated 10 times.'
  write(*,'(/A)') '  x            y            DCPABS(x,y)  Alternative'
  
  do i = 1, n
    x2(i) = random_range_dp(-100.d0,100.d0)
    y2(i) = random_range_dp(-100.d0,100.d0)
    write(*,'(4(F8.3,5X))') x2(i), y2(i), DCPABS(x2(i),y2(i)), abs(cmplx(x2(i),y2(i),kind=dp))
  end do 
  
  write(*,*)
  call wait
end subroutine TEST_DCPABS


subroutine TEST_CREC
! Subroutine to test the NSWC routine CREC.
  integer, parameter :: n = 10
  complex(sp) z1(n)
  real(sp) u1
  real(sp) v1
  integer i
  
  do i = 1, n
    z1(i)%re = random_range_sp(-100.00,100.0)
    z1(i)%im = random_range_sp(-100.00,100.0)
  end do
  
  write(*,'(/A)') 'Test NSWC subroutine CREC(x,y,u,v) '
  write(*,'(A)') '-----------------------------------'
  write(*,'(/A)')'For a random complex number with single precision real and'
  write(*,'(A)') 'imaginary parts x and y, the real and imaginary parts of its'
  write(*,'(A)') 'reciprocal u and v are calculated by calling CREC.  An'
  write(*,'(A)') 'alternative calculation is also preformed.  This is repeated'
  write(*,'(A)') '10 times.' 

  write(*,'(/A)') '                                  CREC                           Direct calculation           '
  write(*,'(A)')  'x                y                re(1/(x+jy)    im(1/(x+jy))    re(1/(x+jy)     im(1/(x+jy)) '
  do i = 1, n
    call CREC( (z1(i)%re), z1(i)%im, u1, v1)
    write(6,'(6(F8.4,8X))') z1(i), u1, v1, 1.0/z1(i)
  end do
  
  write(*,*)
  call wait
  
end subroutine TEST_CREC


subroutine TEST_DCREC
! Subroutine to test the NSWC routine DCREC.
  integer, parameter :: n = 10
  complex(dp) z2(n)
  real(dp) u2
  real(dp) v2
  integer i

  do i = 1, n
    z2(i)%re = random_range_dp(-100.d0,100.d0)
    z2(i)%im = random_range_dp(-100.d0,100.d0)  
  end do
  
  write(*,'(/A)')'Test NSWC subroutine DCREC(x,y,u,v)'
  write(*,'(A)') '-----------------------------------'
  write(*,'(/A)')'For a random complex number with double precision real and'
  write(*,'(A)') 'imaginary parts x and y, the real and imaginary parts of its'
  write(*,'(A)') 'reciprocal u and v are calculated by calling DCREC.  An'
  write(*,'(A)') 'alternative calculation is also preformed.  This is repeated'
  write(*,'(A)') '10 times.' 
  write(*,'(/A)')'                                  DCREC                          Direct calculation           '
  write(*,'(A)') 'x                y                re(1/(x+jy)    im(1/(x+jy))    re(1/(x+jy)     im(1/(x+jy)) '
  
  do i = 1, n
    call DCREC( (z2(i)%re), z2(i)%im, u2, v2)
    write(*,'(6(F8.4,8X))') z2(i), u2, v2, 1.0/z2(i)
  end do
  
  write(*,*)
  call wait
end subroutine TEST_DCREC


subroutine TEST_CDIV
! Subroutine to test the NSWC routine CDIV.
  integer, parameter :: n = 20
  complex(sp) z1(n)
  complex(sp) CDIV              ! Function CDIV must be declared as real in calling routine
  integer i
  
  do i = 1, n
    z1(i)%re = random_range_sp(-100.00,100.0)
    z1(i)%im = random_range_sp(-100.00,100.0)  
  end do
  
  write(*,'(/A)') 'Test NSWC function CDIV(x,y,u,v) '
  write(*,'(A)')  '---------------------------------'
  write(*,'(/A)') 'Here the results of the division of one complex number'
  write(*,'(A)')  '(x+jy) by another (u+jv) is performed using the function'
  write(*,'(A)')  'single precision function CDIV. An alternative calculation'
  write(*,'(A)')  'is also performed.'
  write(*,'(/A)') '                                           CDIV                 Direct claculation'
  
  do i = 1, n, 2
    write(*,'("[",F8.4,",",F8.4,"]",A,"[",F8.4,F8.4,"]",A,"[",F8.4,F8.4,"]",3X,"[",F8.4,F8.4,"]")') &
       z1(i),' / ', z1(i+1), ' = ', CDIV(z1(i),z1(i+1)), z1(i)/z1(i+1)
  end do
  
  write(*,*)
  call wait
end subroutine TEST_CDIV


subroutine TEST_CDIVID
! Subroutine to test the NSWC routine CDIVID.
  integer, parameter :: n = 20
  real(dp) a1(n)
  real(dp) b1(n)
  real(dp) a2(n)
  real(dp) b2(n)
  real(dp) u
  real(dp) v
  integer i
  
  do i = 1, n
    a1(i) = random_range_dp(-100.d0,100.d0)
    b1(i) = random_range_dp(-100.d0,100.d0)
    a2(i) = random_range_dp(-100.d0,100.d0)
    b2(i) = random_range_dp(-100.d0,100.d0)  
  end do
  
  write(*,'(/A)') 'Test NSWC subroutine CDIVID(a1, b1, a2, b2, u, v)'
  write(*,'(A)')  '-------------------------------------------------'
  write(*,'(/A)') 'Here the results of the division of one complex number'
  write(*,'(A)')  '(x+jy) by another (u+jv) is performed using the function'
  write(*,'(A)')  'double precision function CDIVD.  An alternative calculation'
  write(*,'(A)')  'is also performed.'
  write(*,'(/A)') '                                           CDIVID               Direct claculation'
  do i = 1, n, 2
    call CDIVID(a1(i),b1(i),a2(i),b2(i),u,v)
    write(*,'("[",F8.4,",",F8.4,"]",A,"[",F8.4,F8.4,"]",A,"[",F8.4,F8.4,"]",3X,"[",F8.4,F8.4,"]")')&
            cmplx(a1(i),b1(i),kind=dp),' / ',cmplx(a2(i),b2(i),kind=dp), ' = ',u, v,               &
            cmplx(a1(i),b1(i),kind=dp)/cmplx(a2(i),b2(i),kind=dp) 
  end do
  
  write(*,*)
  call wait

end subroutine TEST_CDIVID


subroutine TEST_DCSQRT
! Subroutine to test the NSWC routine DCSQRT.
  integer, parameter :: n = 10
  real(dp) z(2)
  real(dp) w(2)
  integer i
  
  write(*,'(/A)')'Test NSWC subroutine DCSQRT(Z,W)'
  write(*,'(A)') '--------------------------------'
  write(*,'(/A)') 'Here the square root of the a complex number stored with'
  write(*,'(A)')  'its real part in Z(1) and its imaginary part in Z(2), Z'
  write(*,'(A)')  'being a double precision real array, is returned in W(1)'
  write(*,'(A)')  'and W(2), W also being a double precision real array.'
  write(*,'(A)')  'An alternative calculation is also performed.'
  write(*,'(/A)') 'Test value,          DCSQRT,              Direct calculation'
  
  do i = 1, n
    z(1) = random_range_dp(-100.d0,100.d0)
    z(2) = random_range_dp(-100.d0,100.d0)
    CALL DCSQRT(z,w)
    write(*,'(3("[",F8.4,",",F8.4,"]",2X))') z(1), z(2), w(1), w(2), sqrt(cmplx(z(1),z(2),kind=dp))
  end do
  
  write(*,*)
  call wait
end subroutine TEST_DCSQRT


subroutine geometry
! Subroutine to test the folloing NSWC routines: HULL, PAREA, LOCPT and PFIND
  integer, parameter :: m = 10
  real(sp) x(m)
  real(sp) y(m)
  real(sp) xcopy(m)
  real(sp) ycopy(m)
  real(sp) bx(m+1)
  real(sp) by(m+1)
  real(sp) vx(m+1)
  real(sp) vy(m+1)
  real(sp) u(m)
  real(sp) v(m)
  real(sp) randomx
  real(sp) randomy
  real(sp) a(2)
  real(sp) b(2)
  real(sp) linex(2)
  real(sp) liney(2)
  real(sp) area
  integer i
  integer j
  integer iw
  integer k
  integer n
  integer npl(2)
  integer ret
  integer wind
  integer nmax
  integer ierr
  character(len=120) result_string
  real(sp) parea
  
  write(*,'(/A)') 'Test NSWC subroutine HULL'
  write(*,'(A)')  '-------------------------'
  write(*,'(/A)') 'Here a random data set of 10 (x,y) coordinate pairs are'
  write(*,'(A)')  'generated.  The convex hull enclosing these data points'
  write(*,'(A)')  'is found using the single precision subroutine HULL.'
  write(*,'(/A)') 'The original 10 data points and the convex hull are '
  write(*,'(A)')  'plotted using the Clearwin %pl function.' 
  
  do i = 1, m
    x(i) = random_range_sp(-100.0,100.0)
    y(i) = random_range_sp(-100.0,100.0)
  end do
  
  xcopy = x
  ycopy = y
  
  write(*,'(/A)') 'Test data'
  write(*,'(A)') 'x         y'
  do i = 1, m
    write(*,'(F8.4," , ",F8.4)') x(i),y(i)
  end do
  
  CALL HULL(x,y,m,bx,by,k,vx,vy,n)
  
  write(*,'(/A)') 'Points defining convex hull'
  write(*,'(A,1X,I3)')'Number of distinct points in convex hull = ', k-1
  write(*,'(A)') 'x         y'
  do i = 1, k
    write(*,'(F8.4," , ",F8.4)') bx(i),by(i)
  end do

! Calculate the area of the convex hull using the NSWC function PAREA
  area = PAREA(bx,by,k)
  write(result_string,'(A,EN12.3,1X,A)') 'Area of convex hull = ', area, 'sq units'

  write(*,*)
  call wait
  
  npl = [m,k]
  iw = winio@('%ww[topmost]&')
  iw = winio@('%mn[Exit]&','exit')
  iw = winio@('%^tt[Continue]&','exit')
  iw = winio@('%fn[Arial]&')
  iw = winio@('%ts&',1.2d0)
  iw = winio@('%bf&')
  iw = winio@('%nl%cnTest NSWC subroutine HULL%2nl&')
  iw = winio@('%ts&',1.d0)
  iw = winio@('%nl%cnHULL determines minimum convex hull enclosing a set of points%nl&')
  iw = winio@('%ts&',1.2d0)
  call winop@('%pl[native,x_array,n_graphs=2,independent,frame,width=2,smoothing=4,gridlines]')
  call winop@('%pl[link=none,symbol=11,colour=red]')
  call winop@('%pl[link=lines,symbol=0.colour=black]')
  iw = winio@('%pl&',500,500,npl,dble(xcopy),dble(ycopy),dble(bx),dble(by))
  iw = winio@('%ff%cn%ws&',result_string)
  iw = winio@('')
  
  write(*,'(/A)') 'Test NSWC subroutine LOCPT'
  write(*,'(A)')  '--------------------------'
  write(*,'(/A)') 'Here the convex hull generated by the test of HULL is used.'
  write(*,'(A)')  'A random point on the x-y plane is generated.  The LOCPT'
  write(*,'(A)')  'subroutine is used to determine if the generated point is'
  write(*,'(A)')  'outside, on, or inside the convex hull.  This is repeated'
  write(*,'(A)')  '5 times.  The results are also plotted using the Clearwin'
  write(*,'(A)')  '%pl function.'  
  
  do i = 1, 5
    randomx = random_range_sp(-150.0,150.0)
    randomy = random_range_sp(-150.0,150.0)
    
    CALL LOCPT(randomx, randomy,bx,by,k,ret,wind)
    
    if (ret .eq. -1) then
      write(result_string,'("(",F9.4,",",F9.4,")",2X,A)') randomx, randomy, 'is outside the path'
      write(*,'(A)') result_string
    else if (ret .eq. 0) then
      write(result_string,'("(",F9.4,",",F9.4,")",2X,A)') randomx, randomy, 'is on the path'
      write(*,'(A)') result_string
    else if (ret .eq. 1) then
      write(result_string,'("(",F9.4,",",F9.4,")",2X,A)') randomx, randomy, 'is inside the path'
      write(*,'(A)') result_string
    end if

    write(*,*)
    call wait
    
    npl = [k,1]
    iw = winio@('%ww[topmost]&')  
    iw = winio@('%mn[Exit]&','exit')
    iw = winio@('%^tt[Continue]&','exit')
    iw = winio@('%fn[Arial]&')
    iw = winio@('%ts&',1.2d0)
    iw = winio@('%bf&')
    iw = winio@('%nl%cnTest NSWC subroutine LOCPT%2nl&')
    iw = winio@('%ts&',1.d0)
    iw = winio@('%nl%cnLOCPT determines if a point is inside, on, or outside a polygon%nl&')
    iw = winio@('%ts&',1.2d0)
    call winop@('%pl[native,x_array,n_graphs=2,independent,frame,width=2,smoothing=4,gridlines]')
    call winop@('%pl[link=lines,symbol=0,colour=black]')
    call winop@('%pl[link=none,symbol=11,colour=red]')
    iw = winio@('%pl&',500,500,npl,dble(bx),dble(by),dble(randomx),dble(randomy))
    iw = winio@('%ff%cn%ws&',result_string)
    iw = winio@('')
  end do

  write(*,'(/A)') 'Test NSWC subroutine PFIND'
  write(*,'(A)')  '--------------------------'  
  write(*,'(/A)') 'Here the convex hull generated by the test of HULL is used.'
  write(*,'(A)')  'A random line on the x-y plane is generated.  The PFIND'
  write(*,'(A)')  'is used to find the locations of the intersections of this'
  write(*,'(A)')  'line with the convex hull.  This is repeated 5 times.'
  write(*,'(A)')  'The results are also plotted using the Clearwin %pl function.'
  
  nmax =size(u)
  k = npl(1)
  write(*,'(A)') 'Coordinates of the test polygon:'
  do j = 1, k
    write(*,'("(",F8.4,",",F8.4,")")') bx(j), by(j)
  end do
  
  do i = 1, 5
    do j = 1, 2
      a(j) = random_range_sp(-100.0,100.0)
      b(j) = random_range_sp(-100.0,100.0)
    end do
    linex(1) = a(1)
    linex(2) = b(1)
    liney(1) = a(2)
    liney(2) = b(2)
    write(*,*)
    write(*,'(A)')'Coordinates of the test points defining intersecting line'
    write(*,'("(",F8.4,",",F8.4,")")') a(1),a(2) 
    write(*,'("(",F8.4,",",F8.4,")")') b(1),b(2)  
    k = npl(1)
    
    CALL PFIND(a,b,bx,by,npl(1),u,v,nmax,k,ierr)
    
    write(*,*)
    write(*,'(A,I1)') 'IERR = ',ierr
    if (ierr .eq. 0) then
      write(result_string,'(A,"(",F8.4,",",F8.4,")",A,"(",F8.4,",",F8.4,")",1X,A,I2,1X,A)') &
          'Line between ', a(1),a(2), ' and ', b(1),b(2), 'intersets polygon ', k ,'time(s).'
      write(*,'(A)') result_string
      do j = 1, k
        write(*,'("Intersection ",I2," at (", F9.4,",",F9.4,")")') j, u(j),v(j)
      end do 

      write(*,*)
      call wait
      
      npl = [npl(1),2]  
      iw = winio@('%ww[topmost]&')
      iw = winio@('%mn[Exit]&','exit')
      iw = winio@('%^tt[Continue]&','exit')
      iw = winio@('%fn[Arial]&')
      iw = winio@('%ts&',1.2d0)
      iw = winio@('%bf&')
      iw = winio@('%nl%cnTest NSWC subroutine PFIND%2nl&')
      iw = winio@('%ts&',1.d0)
      iw = winio@('%nl%cnPFIND determines the intersection points of a line and a polygon%nl&')
      iw = winio@('%ts&',1.2d0)
      call winop@('%pl[native,x_array,n_graphs=2,independent,frame, width=2,smoothing=4,gridlines]')
      call winop@('%pl[link=lines,symbol=0,colour=black]')
      call winop@('%pl[link=lines,symbol=0,colour=red]')
      iw = winio@('%pl&',500,500,npl,dble(bx),dble(by),dble(linex),dble(liney))
      iw = winio@('%ts&',1.d0)
      iw = winio@('%ff%ws&',result_string)
      iw = winio@('')       
    end if
  end do
end subroutine geometry


subroutine TEST_CERF
! Subroutine to test the NSWC routine CERF
  complex(sp) z1
  complex(sp) w1
  real(sp) x1
  real(sp) y1
  integer i
  integer m
  
  write(*,'(/A)')'Test NSWC subroutine CERF'
  write(*,'(A)') '-------------------------'
  write(*,'(A)') 'The subroutine CERF(M0,Z,W) returns the error function of'
  write(*,'(A)') 'complex argument Z in complex W, when integer M0 is zero.'
  write(*,'(A)') 'When M0 is 1 it returns the complementary error function of'
  write(*,'(A)') 'Z as W.'
  write(*,'(/A)')'Here we test CERF, for a range of complex numbers in which'
  write(*,'(A)') 'the imaginary part is zero.  Consequenty the real part of'
  write(*,'(A)') 'the returned value W should correspond to that returned by'
  write(*,'(A)') 'the intrinsic real function ERF(real(W)) when MO=0, and'
  write(*,'(A/)') 'the intrinsic real function ERFC(real(W)) when M0=1.'
  
  do i = 1, 5
    call random_number(x1)
    y1 = 0.0
    z1 = cmplx(x1,y1)
    m = 0
    ! m is zero so CERF returns ERF(z)
    ! Since aimag(z) is zero, this should be the same as calling the intrinsic ERF(real(z))
    CALL CERF(m,z1,w1)
    write(*,'(A,"(",F8.4,",",F8.4,")",A,I2,1X,A,"(",F8.4,",",F8.4,")",1X,A,F8.4)') &
           'Z = ', z1, ' m = ', m, 'CERF(z) =', w1, 'Intrinsic ERF for real part = ',erf(x1)
  end do
  
  write(*,*)
  
  do i = 1, 5
    call random_number(x1)
    y1 = 0.0
    z1 = cmplx(x1,y1)
    ! m is one so CERF returns the complimentary ERF(z)
    ! Since aimag(z) is zero, this should be the same as calling the intrinsic ERFC(real(z))
    m = 1
    CALL CERF(m,z1,w1)
    write(*,'(A,"(",F8.4,",",F8.4,")",A,I2,1X,A,"(",F8.4,",",F8.4,")",1X,A,F8.4)') &
           'Z = ', z1, ' m = ', m, 'CERF(z) =', w1, 'Intrinsic ERFC for real part = ',erfc(x1)
  end do
  
  write(*,*)
  call wait
end subroutine TEST_CERF


subroutine test_CBSSLJ
! Subroutine to test the NSWC routine CBSSLJ
  integer, parameter :: n = 41
  complex(sp) z(n)
  complex(sp) w(n)
  complex(sp) j0(n)
  complex(sp) j1(n)
  complex(sp) j2(n)
  real(sp) dx
  integer i
  integer iw
  integer pl
  
  write(*,'(/A)') 'Test NSWC subroutine CBSSLJ'
  write(*,'(A)')  '---------------------------'
  write(*,'(/A/)')'Test 1'
  write(*,'(/A)') 'Here we use CBSSLJ to calculate J0(Z) for complex'
  write(*,'(A)')  'Z where the imaginary part is zero and compare the'
  write(*,'(A/)') 'results with the intrinsic real function BESSEL_J0.' 

  dx = 20.0/40
  z(1) = cmplx(0.0,0.0)
  do i = 2, size(z) 
    z(i) = z(i-1) + dx
  end do
  
  write(*,*)  
  call wait
  
  write(*,'(/A)') 'x           CBSSLJ    BESSEL_J0 intrinsic'
  do i = 1, size(z)
    call CBSSLJ(z(i),cmplx(0.0,0.0),w(i))
    write(*,'(3(F8.4,2X))') real(z(i)), real(w(i)), BESSEL_J0(real(z(i)))
  end do
  
  write(*,*)
  call wait
  
  iw = winio@('%ww[topmost]&')
  iw = winio@('%mn[Exit]&','exit')
  iw = winio@('%^tt[Continue]&','exit')
  iw = winio@('%fn[Arial]&')
  iw = winio@('%ts&',1.2d0)
  iw = winio@('%bf&')
  iw = winio@('%nl%cnTest NSWC subroutine CBSSLJ%2nl&')
  call winop@('%pl[native,x_array,n_graphs=1,independent,frame, width=2,smoothing=4]')
  call winop@('%pl[gridlines,link=curves,symbol=0,colour=blue]')
  iw = winio@('%pl&',500,500,size(z,kind=3),dble(real(z)),dble(real(w)))
  iw = winio@('')
  
  write(*,'(/A/)')'Test 2'
  write(*,'(/A)') 'Here we use CBSSLJ to calculate J0(Z),J1(Z) and'
  write(*,'(A)')  'J2(Z) where the imaginary part is zero and plot'
  write(*,'(A/)')  'the results obtained.'

  write(*,*)
  call wait
  
  do i = 1, size(z)
    call CBSSLJ(z(i),cmplx(0.0,0.0),j0(i))
    call CBSSLJ(z(i),cmplx(1.0,0.0),j1(i))
    call CBSSLJ(z(i),cmplx(2.0,0.0),j2(i))
  end do
  
  iw = winio@('%ww[topmost]&')
  iw = winio@('%mn[Exit]&','exit')
  iw = winio@('%^tt[Continue]&','exit')
  iw = winio@('%fn[Arial]&')
  iw = winio@('%ts&',1.2d0)
  iw = winio@('%bf&')
  iw = winio@('%nl%cnTest NSWC subroutine CBSSLJ%2nl&')
  iw = winio@('%cnPlot of Bessel function of the first kind%nl&')
  iw = winio@('%cnJn(x), for integer orders n = 0, 1, 2%2nl&')
  call winop@('%pl[native,x_array,n_graphs=3,frame, width=2,smoothing=4,gridlines]')
  call winop@('%pl[link=curves,symbol=0,colour=blue]')
  call winop@('%pl[link=curves,symbol=0,colour=red]')
  call winop@('%pl[link=curves,symbol=0,colour=green]')
  pl = size(z,kind=3)
  iw = winio@('%cn%tc[blue]Jo  %tc[red]J1  %tc[green]J2  %tc[black]&')
  iw = winio@('%ff%pl&',500,500,[pl,pl,pl],dble(real(z)),dble(real(j0)),dble(real(j1)),dble(real(j2)))
  iw = winio@('')
end subroutine test_CBSSLJ


subroutine test_zeroin
! Subroutine to test the NSWC routine ZEROIN
  integer n,nbmax
  real(sp) x1,x2
  parameter(n=100,nbmax=200,x1=0.0,x2=50.0)
  integer i,nb
  real(sp) xb1(nbmax),xb2(nbmax)
  real(sp) ZEROIN, aerr,rerr, roots(n)
  nb = nbmax
  write(*,'(/A)') 'Test NSWC subroutine ZEROIN'
  write(*,'(A)')  '---------------------------'
  write(*,'(/A)') 'Here we find the zeros of the bessel function of '
  write(*,'(A)')  'the first kind of order 0 i.e. J0(x) for real x in'
  write(*,'(A)')  'the range [0,50].  First we find bracket the roots'
  write(*,'(A)')  'then refine the location of the roots using ZEROIN.'
  nb=NBMAX
  
  ! ZBRAK is not part of NSWC library
  call zbrak(bessj0,X1,X2,N,xb1,xb2,nb)
  
  write(*,'(/A)') 'Brackets for roots of BESSJ0:'
  write(*,'(/A)')  '    lower     upper         F(lower)  F(upper)'
  
  do i = 1, nb
    write(*,'(2(2f10.4,4x))') xb1(i),xb2(i),bessj0(xb1(i)),bessj0(xb2(i))
  end do
  
  ! Now use ZEROIN to refine location of roots
  aerr = 0.0
  rerr = 0.0  ! Machine accuracy requested
  write(*,'(/A/)')'Location of roots from ZEROIN:'
  
  do i = 1, nb
    roots(i) = ZEROIN(bessj0,xb1(i),xb2(i),aerr,rerr)
    write(*,'(A,F8.4,A,F8.4,A,F8.4)') 'Root between ',xb1(i),' and ', xb2(i), ' is at ', roots(i) 
  end do
  
  write(*,*)
  call wait  
end subroutine test_zeroin


subroutine test_qdcrt
! Subroutine to test the NSWC routine QDCRT
  integer, parameter :: n = 2
  real(sp)  a(0:n)
  complex(sp) z(n) 
  integer  i, j
  
  write(*,'(/A)') 'Test NSWC subroutine QDCRT'
  write(*,'(A)')  '--------------------------'
  write(*,'(/A)') 'Here we generate a quadratic equation with random'
  write(*,'(A)')  'coefficients, and then use QDCRT to find the roots'
  write(*,'(A)')  'of the quadratic equation, which may be real or '
  write(*,'(A)')  'or complex.'
  write(*,'(A)')  'As a check on the returned roots, the quadratic is'
  write(*,'(A)')  'is evaluated at the returned roots.'
  
  do j = 1, 5
    do i = 0, n
      a(i) = random_range_sp(-100.0, 100.0)
    end do
    
    CALL QDCRT(a,z)
    
    write(*,'(/A)') 'The roots of the quadratic equation:'
    write(*,'(/"y = ",F8.4," + ",F8.4,"x + ",F8.4,"x**2")') a(0), a(1), a(2)
    write(*,'(/A/)') 'are:'
    
    do i = 1, n
      write(*,'(I2,3X,"(",F8.4,",",F8.4,")")') i, z(i)
    end do 
    
    do i = 1, n
      write(6,'(/A,"(",F8.4,",",F8.4,")",A,"(",F8.4,",",F8.4,")")') &
        'At x = ', z(i), ' the value of y is ', a(0)+a(1)*z(i) + a(2)*z(i)*z(i)
    end do
    
    write(*,*)
    call wait
    
  end do
end subroutine test_qdcrt


subroutine test_crout1
! Subroutine to test the NSWC routine CROUT.
  integer, parameter :: sp=kind(1.0)
  integer, parameter :: n  = 4  ! Rank of A, i.e A is 4x4
  integer, parameter :: m  = 1  ! No of columns in B
  integer ,parameter :: m0 = 0  ! Flag for inverse computation
  integer, parameter :: ka = 4  ! No of rows in A
  integer, parameter :: kb = 4  ! No of rows in B
  real(sp) :: a(n,n)
  real(sp) :: acopy(n,n)
  real(sp) :: b(n,m) = 0.0      ! B is not used by CROUT when m0 = 0
  real(sp) :: d
  real(sp) :: temp(n)
  real(sp) :: idcheck(n,n)
  integer  :: index1(n)
  integer  :: i
  integer  :: j
  write(*,'(/A)') 'Test NSWC subroutine CROUT'
  write(*,'(A)')  '--------------------------' 
  write(*,'(/A)') 'Here we invert the matrix A using the subroutine'
  write(*,'(A)')  'CROUT.  A is a real matrix with 4 rows and 4 columns.'

! Define the A matrix
  a = reshape((/ 2.0,  1.0, -1.0,  1.0, &
                -3.0, -1.0,  2.0,  2.0, &
                -2.0,  1.0,  2.0, -1.0, &
                 1.0,  2.0,  1.0, -3.0/),shape(a))

! A will be changed when CROUT returns so make a copy
  acopy = a
  
  write(*,'(/A/)') 'Original matrix A'
  do i = 1, n
    write(*,'(4(F8.4,2X))') (a(i,j), j=1,n)
  end do

! Invert A by calling CROUT
  CALL CROUT(m0,n,m,a,ka,b,kb,d,index1,temp)

! Output calculated determinant and inverse of A to screen
  write(*,'(/A,F8.4)') 'Determinant of A returned by CROUT ', D
  write(*,'(/A/)') 'Inverse A returned by CROUT'
  do i = 1, n
    write(*,'(4(F8.4,2X))') (a(i,j), j=1,n)
  end do

! Determine the matrix produce of original A and inverse.
  idcheck = matmul(acopy,a)
  write(*,'(/A)') 'Matrix multiplication of original A and returned inverse.'
  write(*,'(A/)') 'This should equal the identity matrix.'
  do i = 1, n
    write(*,'(4(F8.4,2X))') (idcheck(i,j), j=1,n)
  end do
  
  write(*,*)
  call wait
end subroutine test_crout1

  
subroutine test_crout2
! Subroutine to test the NSWC routine CROUT.
  integer, parameter :: sp=kind(1.0)
  integer, parameter :: n  = 4  ! Rank of A, i.e A is 4x4
  integer, parameter :: m  = 1  ! No of columns in B
  integer ,parameter :: m0 = 1  ! Flag for no inverse computation
  integer, parameter :: ka = 4  ! No of rows in A
  integer, parameter :: kb = 4  ! No of rows in B
  real(sp) a(n,n)
  real(sp) acopy(n,n)
  real(sp) b(n,m)
  real(sp) bcopy(n,m)
  real(sp) d
  real(sp) temp(n)
  real(sp) x(n,m)
  real(sp) bcheck(n,m)
  integer index1(n)
  integer i
  integer j
  
  write(*,'(/A)') 'Test NSWC subroutine CROUT'
  write(*,'(A)')  '--------------------------' 
  write(*,'(/A)') 'Here we solve the matrix equation AX = B'
  write(*,'(A)')  'A is a matrix with 4 rows and 4 columns.'
  write(*,'(A)')  'B is a matrix with 4 rows and 1 column.'
  write(*,'(A)')  'X is returned in B, and A is destroyed.'

! Define the A matrix
  a = reshape((/ 2.0,  1.0, -1.0,  1.0, &
                -3.0, -1.0,  2.0,  2.0, &
                -2.0,  1.0,  2.0, -1.0, &  
                 1.0,  2.0,  1.0, -3.0/),shape(a))

! Define the B matrix
  b = reshape((/ 5.0, -1.0, 2.0, -4.0/),shape(b))

! Both A and B will be changed when CROUT returns so make copies
  acopy = a
  bcopy = b
  
! Write A to screen
  write(*,'(/A/)') 'Original matrix A'
  do i = 1, n
    write(*,'(4(F8.4,2X))') (acopy(i,j), j=1,n)
  end do

! Write B to screen
  write(*,'(/A/)') 'Matrix B'
  do i = 1, n
    write(*,'(4(F8.4,2X))') (bcopy(i,j), j=1,m)
  end do
  
! Solve by calling CROUT
  CALL CROUT(m0,n,m,a,ka,b,kb,d,index1,temp)
      
! Copy returned B to X
  x = b

! Write solution X to screen
  write(*,'(/A/)') 'Solution vector X after CROUT'
  do i = 1, n
    write(*,'(4(F8.4,2X))') (x(i,j), j=1,m)
  end do

! As check calculate the matrix product of original A with X, which should equal original B.
  bcheck = matmul(acopy,x)
  write(*,'(/A/)') 'Matrix multiplication of original A and solution vector X'
  do i = 1, n
    write(*,'(4(F8.4,2X))') (bcheck(i,j), j=1,m)
  end do
  
  write(*,*)
  call wait
end subroutine test_crout2

  
subroutine test_cbcrt
! Subroutine to test the NSWC routine CBCRT
  integer, parameter :: n = 3
  real(sp)    :: a(0:n)
  complex(sp) :: z(n) 
  integer     :: i
  integer     :: j
  
  write(*,'(/A)')'Test NSWC subroutine CBCRT'
  write(*,'(A)') '--------------------------'
  write(*,'(/A)') 'Here we generate a cubic equation with random'
  write(*,'(A)')  'coefficients, and then use QDCRT to find the roots'
  write(*,'(A)')  'of the cubic equation, which may be real or '
  write(*,'(A)')  'or complex.'
  write(*,'(A)')  'As a check on the returned roots, the cubic is'
  write(*,'(A)')  'is evaluated at the returned roots.'
  
  do j = 1, 5
    do i = 0, n
      a(i) = random_range_sp(-100.0, 100.0)
    end do
    
    CALL CBCRT(a,z)
    
    write(*,'(/A)') 'The roots of the cubic equation:'
    write(*,'(/"y = ",F8.4," + ",F8.4,"x + ",F8.4,"x**2 + ",F8.4,"x**3")') &
          a(0),a(1),a(2),a(3)
    write(*,'(/A/)') 'are:'
    
    do i = 1, n
      write(*,'(I2,3X,"(",F8.4,",",F8.4,")")') i, z(i)
    end do 
    
    do i = 1, n
      write(6,'(/A,"(",F8.4,",",F8.4,")",A,"(",F8.4,",",F8.4,")")') &
        'At x = ', z(i), ' the value of y is ', a(0)+a(1)*z(i)+a(2)*z(i)*z(i)+a(3)*z(i)*z(i)*z(i)
    end do
    
    write(*,*)
    call wait
    
  end do
end subroutine test_cbcrt


subroutine test_krout1
! Subroutine to test the NSWC routine KROUT.
  integer, parameter :: n = 4
  real(sp) a(n,n)
  real(sp) a_copy(n,n)
  real(sp) b(n)
  real(sp) work(n)
  real(sp) prod(n,n)
  integer m0
  integer m
  integer ka
  integer kb
  integer id(n)
  integer i
  integer j
  integer ierr
 
  write(*,'(/A)') 'Test NSWC subroutine KROUT'
  write(*,'(A)') '--------------------------' 
  write(*,'(/A/)') 'Here we invert a real 4 x 4 matrix.' 
  a = reshape((/ 2.0,  1.0, -1.0,  1.0, &
                -3.0, -1.0,  2.0,  2.0, &
                -2.0,  1.0,  2.0, -1.0, &
                 1.0,  2.0,  1.0, -5.0 /), shape(a))
  a_copy = a
  
  m0 = 0    !Flag for the inverse to be calculated
  m  = 0    !No of columns in b when m>0, but b is not used when m0 = 0
  ka = n    !No of rows in A
  kb = n    !No of rows in B. If (m .le. 0) kb is ignored.
  
  CALL KROUT(m0,n,m,a,ka,b,kb,ierr,id,work)
  
  prod = matmul(a,a_copy)
  
  write(*,'(/A/)') 'Original matrix:'
  do i = 1, n
    write(*,'(4(F8.4,2X))') (a_copy(i,j), j=1,n)
  end do
  
  write(*,'(/A/)') 'Inverse of original matrix:'
  do i = 1, n
    write(*,'(4(F8.4,2X))') (a(i,j), j=1,n)
  end do
  
  write(*,'(/A/)') 'Matrix product of original matrix and its calculated inverse:'
  do i = 1, n
    write(*,'(4(F8.4,2X))') (prod(i,j), j=1,n)
  end do  
  write(*,*)
  call wait
end subroutine test_krout1


subroutine test_krout2
! Subroutine to test the NSWC routine KROUT.
  integer, parameter :: n = 4
  real(sp) a(n,n)
  real(sp) a_copy(n,n)
  real(sp) b(n)
  real(sp) work(n)
  real(sp) x(n)
  integer m0
  integer m
  integer id(n)
  integer i
  integer j
  integer ierr
  integer ka
  integer bk
  
  write(*,'(/A)') 'Test NSWC subroutine KROUT'
  write(*,'(A)') '--------------------------' 
  write(*,'(/A/)') 'Here we solve the matrix equation Ax = B.'
  a = reshape((/ 2.0,  1.0, -1.0,  1.0, &
                -3.0, -1.0,  2.0,  2.0, &
                -2.0,  1.0,  2.0, -1.0, &
                 1.0,  2.0,  1.0, -5.0 /), shape(a))
  b = (/ 5.0, -1.0, 2.0, -4.0/)                 
  a_copy = a

  write(*,'(/A/)') 'Original matrix A' 
  do i = 1, n
    write(*,'(4(F8.4,2X))') (a(i,j), j=1,n)
  end do
  
  write(*,'(/A/)') 'Right hand side vector B' 
  write(*,'(F8.4)') (b(j), j=1,n)
  
  m0 = 0
  m  = 1
  ka = n
  bk = n
  CALL KROUT(m0,n,m,a,ka,b,bk,ierr,id,work)

  x = b        !Solution vector x returned as b
  
  write(*,'(/A/)') 'Solution vector X' 
  write(*,'(F8.4)') (x(j), j=1,n)
      
! Check the solution
  write(6,'(/A/)') 'Check that the dot_product of each row of a with x equals the original RHS b'
    do i = 1, n
        write(*, '(A,F8.4)') 'Ax = ', dot_product(a_copy(i,:), x)
    end do
  write(*,*)
  call wait
end subroutine test_krout2


subroutine test_cmslv
! Subroutine to test the NSWC routine CMSLV.
  real(sp)    :: pi
  real(sp)    :: deg2rad
  real(sp)    :: rcond
  complex(sp) :: jc
  complex(sp) :: h
  complex(sp) :: a(3,3)
  complex(sp) :: a_copy(3,3)
  complex(sp) :: c1
  complex(sp) :: ainv(3,3)
  complex(sp) :: b(3)
  complex(sp) :: d(2)
  complex(sp) :: work(3)
  complex(sp) :: prod(3,3)
  integer     :: i
  integer     :: j
  integer     :: ierr
  integer     :: ipvt(3)
  integer     :: ka
  integer     :: kb
  integer     :: n
  integer     :: m
  integer     :: m0
  write(*,'(/A)') 'Test NSWC subroutine CMSLV'
  write(*,'(A)')  '--------------------------' 
  write(*,'(/A/)')'Here we invert the complex sequence component matrix.'
  write(*,'(A/)') 'See: https://en.wikipedia.org/wiki/Symmetrical_components'

! Generate the required A matrix  
  pi      = 4.0*atan(1.0)
  jc      = cmplx(0.0,1.0)
  deg2rad = pi/180.0
  h       = exp(jc*120.0*deg2rad)
  c1      = (1.0,0.0)
  a = reshape([c1,c1,c1,c1,h,h*h,c1,h*h,h],shape(a))
  
  write(*,'(A/)') 'The sequence component Aabc matrix is:' 
  do i = 1, 3
    write(*,'(3("(",F8.4,",",F8.4,")"))') (a(i,j), j=1,3)
  end do
  
  a_copy = a
  ainv   = reshape([c1,c1,c1,c1,h*h,h,c1,h,h*h],shape(a))/3.0 

  write(*,'(/A/)') 'The inverse of the sequence component Aabc matrix is:' 
  do i = 1, 3
    write(*,'(3("(",F8.4,",",F8.4,")"))') (ainv(i,j), j=1,3)
  end do
  
  write(*,*)
  n  = 3
  m0 = 0
  m  = 0
  ka = n
  kb = n
!            MO, n, m, A, ka, b, kb, D, RCOND, ierr, ipvt, wk
  CALL CMSLV(m0, n, m, a, ka, b, kb, d, rcond, ierr, ipvt, work)

  write(*,'(/A)') 'The inverse of the sequence component Aabc matrix'
  write(*,'(A/)') 'calculated using CMSLV is:'
  
  do i = 1, 3
    write(*,'(3("(",F8.4,",",F8.4,")"))') (a(i,j), j=1,3)
  end do

  prod = matmul(a,a_copy)
  write(*,'(/A/)') 'The product of Aabc and its calculated inverse is:' 
  do i = 1, 3
    write(*,'(3("(",F8.4,",",F8.4,")"))') (prod(i,j), j=1,3)
  end do
  write(*,'(/A/)') 'This is the identity matrix.'
  
  call wait
end subroutine test_cmslv


subroutine test_dqags
! Subroutine to test the NSWC routine CERF
  integer, parameter :: L = 200, M=4*L
  integer i, ierr, num, n
  integer iwk(L)
  double precision wk(M)
  double precision a, b, aerr, rerr, error, z, exact, delta
  character(len=120) :: message(0:6) = ['Integral has been computed to the desired accuracy.          ', &
                                        'More subintervals required to reach the desired accuracy.    ', &
                                        'Roundoff error may impact the accuracy of the result.        ', &
                                        'Extremely bad integrand behavior in the integration interval.', &
                                        'The algorithm did not converge.                              ', &
                                        'The integral may be diverjent or converge extremely slowly   ', &
                                        'Input error.                                                 ']
  double precision funt, x
  funt(x) = 4.d0*x*((x**2)-7.d0)*sin(x)-((x**4)-14.d0*(x**2)+28.d0)*cos(x)
    
  write(*,'(/A)') 'Test NSWC subroutine DQAGS'
  write(*,'(A/)') '--------------------------'
  write(*,'(A)')  'Here we use the subroutine DQAGS to calculate'
  write(*,'(A)')  'the integral of the following function between'
  write(*,'(A)')  'between different limits.'
  write(*,'(/A/)')'f(x) = 4*x*((x**2)-7)*sin(x)-((x**4)-14*(x**2)+28)*cos(x)'
  write(*,'(A)')  'The result obtained from DQAGS is compared '
  write(*,'(A)')  'with the exact solution.'

  a = 0.d0
  b = 4.d0*atan(1.d0)/10.d0
  aerr = 0.000d0
  rerr = 0.000d0
  n=0 
  do i = 1, 12
     call DQAGS(fun, a, b, aerr, rerr, z, error, num, ierr, L, m, n, iwk, wk)
     exact = funt(b) - funt(a)
     delta = z - exact
     write(*,*)
     write(*,'(A22,F16.12)') 'Lower limit           ', a
     write(*,'(A22,F16.12)') 'Upper limit           ', b
     write(*,'(A22,F16.12)') 'Solution from DQAGS   ', z
     write(*,'(A22,F16.12)') 'Exact solution        ', exact
     write(*,'(A22,F16.12)') 'Delta                 ', delta
     write(*,'(A22,I3)')     'Number of f(x) calls  ', num      
     write(*,'(A22,I2)')     'IERR                  ', ierr
     write(*,'(A/)')          message(ierr)
     b = b + 4.d0*atan(1.d0)/10.d0
     call wait
  end do
  
end subroutine test_dqags


subroutine test_fmin
! Subroutine to test the NSWC routine FMIN
  real(sp) :: a
  real(sp) :: b
  real(sp) :: x
  real(sp) :: w
  real(sp) :: aerr
  real(sp) :: rerr
  real(sp) :: error
  integer, parameter :: n = 201
  real(sp) :: xpl(n)
  real(sp) :: ypl(n)
  character(len=120) :: plst
  integer :: ind
  integer :: iw
  integer :: i
  
  ! TEST 1  
  write(*,'(/A)') 'Test NSWC subroutine FMIN'
  write(*,'(A/)') '-------------------------'
  write(*,'(A)')  'Here we find the minimum of the following function:'
  write(*,'(/A/)')'f(x) = (x-1.0)**2 + 2.0'
  write(*,'(A/)') 'This is a convex unimodal function.'
  call wait
  
  a = -10.0
  b =  10.0
  aerr = 1.0e-2
  rerr = 0   ! Request machine precision
  
  call FMIN(fmin_fun,a,b,x,w,aerr,rerr,error,ind)
  
  write(plst,'(2(A,F8.4,1X))') 'Minimum at x = ', x, 'where f(x) = ', w
  do i = 1, n
    if (i .eq. 1) xpl(i) = a
    if (i .ne. 1) xpl(i) = xpl(i-1) + (b-a)/(n-1)
    ypl(i) = fmin_fun(xpl(i))  
  end do
  
  iw = winio@('%ww[topmost]&')
  iw = winio@('%mn[Exit]&','exit')
  iw = winio@('%^tt[Continue]&','exit')
  iw = winio@('%fn[Arial]&')
  iw = winio@('%ts&',1.2d0)
  iw = winio@('%bf&')
  iw = winio@('%nl%cnTest NSWC function FMIN%2nl&')
  iw = winio@('%cnf(x) = (x-1.0)**2 + 2.0%2nl&')
  call winop@('%pl[native,frame,smoothing=4,x_array,independent]')
  call winop@('%pl[width=2,gridlines,n_graphs=2]')
  call winop@('%pl[link=lines,colour=black,symbol=0]')
  call winop@('%pl[link=none, colour=red,  symbol=6]')
  iw = winio@('%pl&',500,500,[n,1],dble(xpl),dble(ypl),dble(x),dble(w))
  iw = winio@('%ff%cn%tc[red]%ws&',plst)
  iw = winio@('')

  ! TEST 2
  write(*,'(/A)') 'Test NSWC subroutine FMIN'
  write(*,'(A/)') '-------------------------'
  write(*,'(A)')  'Here we find the minimum of the following function:'
  write(*,'(/A/)')  'f(x) = -(x + sin(x)) * exp(-x**2.0)'
  write(*,'(A/)') 'This is a non-convex unimodal function.'
  call wait 
  
  a = -10.0
  b =  10.0
  aerr = 1.0e-2
  rerr = 0   ! Request machine precision
  
  call FMIN(fmin_fun2,a,b,x,w,aerr,rerr,error,ind)
  
  write(plst,'(2(A,F8.4,1X))') 'Minimum at x = ', x, 'where f(x) = ', w
  do i = 1, n
    if (i .eq. 1) xpl(i) = a
    if (i .ne. 1) xpl(i) = xpl(i-1) + (b-a)/(n-1)
    ypl(i) = fmin_fun2(xpl(i))  
  end do

  iw = winio@('%ww[topmost]&')
  iw = winio@('%mn[Exit]&','exit')
  iw = winio@('%^tt[Continue]&','exit')
  iw = winio@('%fn[Arial]&')
  iw = winio@('%ts&',1.2d0)
  iw = winio@('%bf&')
  iw = winio@('%nl%cnTest NSWC function FMIN%2nl&')
  iw = winio@('%cnf(x) = -(x + sin(x)) * exp(-x**2)%2nl&')
  call winop@('%pl[native,frame,smoothing=4,x_array,independent]')
  call winop@('%pl[width=2,gridlines,n_graphs=2]')
  call winop@('%pl[link=lines,colour=black,symbol=0]')
  call winop@('%pl[link=none, colour=red,  symbol=6]')
  iw = winio@('%pl&',500,500,[n,1],dble(xpl),dble(ypl),dble(x),dble(w))
  iw = winio@('%ff%cn%tc[red]%ws&',plst)
  iw = winio@('')

  ! WRITE TEST 3
  write(*,'(/A)') 'Test NSWC subroutine FMIN'
  write(*,'(A/)') '-------------------------'
  write(*,'(A)')  'Here we find the minimum of the following function:'
  write(*,'(/A/)')  'f(x) = sin(x) + sin((10.0 / 3.0) * x)'
  write(*,'(A/)') 'This is a non-convex muitimodal function.'
  call wait
  
  a = -10.0
  b =  10.0
  aerr = 1.0e-2
  rerr = 0   ! Request machine precision
  
  call FMIN(fmin_fun3,a,b,x,w,aerr,rerr,error,ind)
  
  write(plst,'(2(A,F8.4,1X))') 'Minimum at x = ', x, 'where f(x) = ', w
  do i = 1, n
    if (i .eq. 1) xpl(i) = a
    if (i .ne. 1) xpl(i) = xpl(i-1) + (b-a)/(n-1)
    ypl(i) = fmin_fun3(xpl(i))  
  end do
  
  iw = winio@('%ww[topmost]&')
  iw = winio@('%mn[Exit]&','exit')
  iw = winio@('%^tt[Continue]&','exit')
  iw = winio@('%fn[Arial]&')
  iw = winio@('%ts&',1.2d0)
  iw = winio@('%bf&')
  iw = winio@('%nl%cnTest NSWC function FMIN%2nl&')
  iw = winio@('%cnf(x) = sin(x) + sin((1 / 3) * x)%2nl&')
  call winop@('%pl[native,frame,smoothing=4,x_array,independent]')
  call winop@('%pl[width=2,gridlines,n_graphs=2]')
  call winop@('%pl[link=lines,colour=black,symbol=0]')
  call winop@('%pl[link=none, colour=red,  symbol=6]')
  iw = winio@('%pl&',500,500,[n,1],dble(xpl),dble(ypl),dble(x),dble(w))
  iw = winio@('%ff%cn%tc[red]%ws&',plst)
  iw = winio@('%tc[black]%2nl%cnCheck graph FMIN often fails for muitimodal functions&')
  iw = winio@('')
  
end subroutine test_fmin

  
subroutine test_DRCIR
! Subroutine to test the NSWC routine DRCIR
  integer, parameter :: nmax = 1000
  real(dp) x(nmax)
  real(dp) y(nmax)
  integer i
  integer ix
  integer ierr
  integer n
  integer iw
  integer iostat
  character(len=60) pl_string
  write(*,'(/A)') 'Test NSWC function DRCIR'
  write(*,'(A/)') '------------------------'
  write(*,'(A)')  'Generates a set of points uniformly across the'
  write(*,'(A)')  'unit circle.'
    
! Set seed for random number generator 
  ix = 1   
  do i = 1, 4
    iostat = 999
    do while (iostat .ne. 0)
      write(*,'(/A)') 'Enter number of points in range [2,1000]:'
      read(*,*,iostat=iostat) n
    end do
    if (n.lt. 2) then
      n = 2
    else if (n.gt. 1000) then
      n = 1000
    end if
    write(pl_string,'(A,I4)')'Number of points = ', n
    
    call DRCIR(n,ix,x,y,ierr)
    
    iw = winio@('%ww[topmost]&')
    iw = winio@('%mn[Exit]&','exit')
    iw = winio@('%^tt[Continue]&','exit')
    iw = winio@('%fn[Arial]&')
    iw = winio@('%ts&',1.2d0)
    iw = winio@('%bf&')
    iw = winio@('%nl%cnTest NSWC function DRCIR%2nl&')
    iw = winio@('%cn%ws%2nl&',pl_string)
    call winop@('%pl[native,frame,smoothing=4,x_array,independent]')
    call winop@('%pl[width=2,gridlines,n_graphs=1]')
    call winop@('%pl[link=none, colour=black, symbol=6]')
    call winop@('%pl[x_max=1,x_min=-1,y_max=1,y_min=-1]')
    iw = winio@('%pl&',500,500,n,x,y)
    iw = winio@('')
  end do
  
end subroutine test_DRCIR


real(sp) function fmin_fun(x)
! Test function used in subroutine TEST_FMIN
real(sp) x
  fmin_fun = ((x-1.0)**2 ) + 2.0
end function fmin_fun


real(sp) function fmin_fun2(x)
! Test function used in subroutine TEST_FMIN
real(sp) x
  fmin_fun2 = -(x + sin(x)) * exp(-x**2.0)
end function fmin_fun2


real(sp) function fmin_fun3(x)
! Test function used in subroutine TEST_FMIN
real(sp) x
  fmin_fun3 = sin(x) + sin((10.0 / 3.0) * x)
end function fmin_fun3


real(dp) function fun(x)
! Test function used in TEST_DQAGS
  real(dp) x
  fun = (x**2)*(x**2-2.d0)*sin(x)
end function fun


real(sp) function bessj0(x)
! This function returns the bessel function j1(x) for real argument x
! It calls the NSWC routine CBSSJL.
real(sp), intent(in) :: x
complex(sp) z, w
  z = cmplx(x,0.0)
  call CBSSLJ(z,cmplx(0.0,0.0),w)
  bessj0 = real(w)
end function bessj0


subroutine zbrak(fx,x1,x2,n,xb1,xb2,nb)
! Routine for bracketing roots.
  integer n,nb
  real(sp) x1,x2,xb1(nb),xb2(nb),fx
  external fx
  integer i,nbb
  real(sp) dx,fc,fp,x
  nbb = 0
  x   = x1
  dx  = (x2 - x1)/n
  fp  = fx(x)
  do i = 1, n
    x  = x + dx
    fc = fx(x)
    if( fc*fp .le. 0.) then
      nbb      = nbb + 1
      xb1(nbb) = x - dx
      xb2(nbb) = x
      if(nbb .eq. nb) exit
    endif
    fp = fc
  end do
  nb = nbb
end subroutine zbrak


real(sp) function random_range_sp(a, b)
! This real function generates a random number in the range [a,b]
  real(sp) :: a, b
  real(sp) :: rand
  call random_number(rand)
  random_range_sp = a + rand * (b - a)
end function random_range_sp


real(dp) function random_range_dp(a, b)
! This double precision function generates a random number in the range [a,b]
  real(dp) :: a, b
  real(dp) :: rand
  call random_number(rand)
  random_range_dp = a + rand * (b - a)
end function random_range_dp

    
subroutine wait
! Haults execution and waits on the user pressing <CR>
  character(len=1) str
  do
    write(*,'(A))',advance='no') '*** Enter <CR> to continue '
    read(*,'(A)') str
    if (len_trim(str) .eq. 0) exit
  end do
end subroutine wait

end module test


program main
use test
   call test_shell
   call test_dsort
   call test_qsortd
   call test_cbrt
   call test_dcbrt
   call test_artnq
   call test_dartnq
   call test_cpabs
   call test_dcpabs
   call test_crec
   call test_dcrec
   call test_cdiv
   call test_cdivid
   call test_dcsqrt
   call test_cerf
   call test_zeroin
   call test_qdcrt
   call test_cbcrt
   call test_crout1 
   call test_crout2 
   call test_krout1
   call test_krout2 
   call test_cmslv
   call test_dqags
   call test_fmin
   call geometry
   call test_cbsslj
   call test_drcir
   write(*,'(/A)')  "**** All tests completed ****"
end program main
