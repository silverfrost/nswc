
Naval Surface Warfare Center (NSWC) LIBRARY OF MATHEMATICS SUBROUTINES
----------------------------------------------------------------------

The NSWC library is a library of general purpose Fortran subroutines that provide
a basic computational capability for a variety of mathematical activities. Emphasis has
been placed on the transportability of the codes. Subroutines are available in the following
areas: elementary operations, geometry, special functions, polynomials, vectors, matrices,
large dense systems of linear equations, banded matrices, sparse matrices, eigenvalues and
eigenvectors, solution of linear equations, least-squares solution of linear equations, 
optimization, transforms, approximation of functions, curve fitting, surface fitting, manifold
fitting, numerical integration, integral equations, ordinary differential equations, partial
differential equations, and random number generation.

The NSWC functions and subroutines are available for general use. The library contains no 
proprietary code


Silverfrost FTN95 NSWC DLLS
-----------------------------------------

Presented here are 32- and 64-bit DLLs containing the NSWC library that have been compiled for use
with FTN95.


Available files
---------------

nswc.for     - the source code (1993 version) used to produce the dlls.  It is not necessary to 
               compile this file if the provided dlls are used.

nswc32.dll   - this should be linked with applications built with the FTN95 32 bit compiler v8.97.2

nswc64.dll   - this should be linked with applications built with the FTN95 63 bit compiler v8.97.2

nswctest.f95 - a Fortran program which compiled with either the 32 or 64 bit FTN95 compiler, and
               linked with the appropriate dll.

               This Fortan program performs a series of simple tests on a number of the routines 
               available in the library.

ADA476840.pdf - the 1990 version of the NSWC users manual.

a261511.pdf   - the 1993 version of the NSWC users manual.



Compiling and running nswctest.ftn
----------------------------------

From the command line, for the 32 bit compiler:

1) compile the code:

  FTN95 nswctest.f95

2) link the obj file with the 32 bit dll

  SLINK nswctest.obj nswc32.dll

3) run the executable:

   nswctest.exe



From the command line, for the 64 bit compiler:

1) compile the code

  FTN95 nswctest.f95 /64

2) link the obj file with the 64 bit dll

  SLINK64 nswctest.obj nswc64.dll

3) run the excutable:

   nswctest.exe



From within the Silverfrost IDE Plato:

1) Open the file nswctest.f95 in Plato

2) In the VIEW tab, ensure that the following are selected: Project Explorer, Standard Tool Bar, and Build Tool Bar 

3) Select either the WIN32 or X64 compiler in the Standard Tool Bar.  

4) In the Project Explorer window, right click on References, and select Add References.  
   This will open a dialogue which allows you to select:

   nswc32.dll for the 32 bit compiler WIN32

   or

   nswc64.dll for the 64 bit compiler X64

5) Use the Compile button in the Build Tool Bar to compile nswctest.f95

6) Use the Build button in the Build Tool Bar to compile nswctest.f95 and link the object file with the dll selected in the References window.

7) Use the Start button in the Build Tool Bar to execute the program.

